from django.shortcuts import render

from app.bysykkel import bs_api

# Create your views here.
def Liste(request):
    '''
        Henter bysykkeldata og viser det fram i bysykkel.html
    '''
    stations = bs_api.getStations()

    return render(request, 'bysykkel.html', {'stations': stations.items()})
