from django.apps import AppConfig


class BysykkelConfig(AppConfig):
    name = 'bysykkel'
