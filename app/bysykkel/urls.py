from django.urls import path

from app.bysykkel.views import Liste

# API
from app.bysykkel import entrypoints 

urlpatterns = [
    path('liste', Liste),
    entrypoints.Stations.url(),
]
