import json

from django.test import TestCase
from app.bysykkel import bs_api

import httpretty

def mockCallback(request, uri, headers):
    '''
        Returnerer et emulert svar fra bysykkel-apiet basert på
        hvilken url som blir kalt
    '''
    response = [200, headers]

    if uri == bs_api.API_URL + '/stations':
        response.append(json.dumps({
            'stations': [
                {'id': 1, 'title': 'Sted'}, {'id': 2, 'title': 'Annet sted'},
                {'id': 3, 'title': 'Stort sted'}, {'id': 4, 'title': 'Lite sted'},
            ],
        }))
    elif uri == bs_api.API_URL + '/stations/availability':
        response.append(json.dumps({
            'stations': [
                {'id': 1, 'availability': {
                    'bikes': 0, 'locks': 20, 'overflow_capacity': False
                }},
                {'id': 2, 'availability': {
                    'bikes': 1, 'locks': 18, 'overflow_capacity': False
                }},
                {'id': 3, 'availability': {
                    'bikes': 2, 'locks': 12, 'overflow_capacity': True
                }},
                {'id': 4, 'availability': {
                    'bikes': 3, 'locks': 21, 'overflow_capacity': False
                }},
            ],
        }))

    return response

# Create your tests here.
class BysykkelTests(TestCase):
    @httpretty.activate
    def test_station_names(self):
        '''
            Tester at _getStationNames funksjonen kombinerer id og navn
            riktig
        '''
        httpretty.register_uri(
            httpretty.GET, bs_api.API_URL + '/stations', body=mockCallback
        )

        station_names = bs_api._getStationNames()

        self.assertEqual(station_names, {
            1: 'Sted', 2: 'Annet sted', 3: 'Stort sted', 4: 'Lite sted',
        })

    @httpretty.activate
    def test_availability(self):
        '''
            Tester at _getAvailability funksjonen kombinerer id og
            informasjon riktig
        '''
        httpretty.register_uri(
            httpretty.GET, bs_api.API_URL + '/stations/availability', body=mockCallback
        )
        availability = bs_api._getAvailability()

        self.assertEqual(availability, {
            1: {'bikes': 0, 'locks': 20, 'overflow_capacity': False},
            2: {'bikes': 1, 'locks': 18, 'overflow_capacity': False},
            3: {'bikes': 2, 'locks': 12, 'overflow_capacity': True},
            4: {'bikes': 3, 'locks': 21, 'overflow_capacity': False},
        })

    @httpretty.activate
    def test_combined(self):
        '''
            Tester at getStations kombinerer informasjonen fra
            _getStationNames og _getAvailability riktig
        '''
        httpretty.register_uri(
            httpretty.GET, bs_api.API_URL + '/stations', body=mockCallback
        )
        httpretty.register_uri(
            httpretty.GET, bs_api.API_URL + '/stations/availability', body=mockCallback
        )

        stations = bs_api.getStations()

        self.assertEqual(stations, {
            'Annet sted': {'bikes': 1, 'locks': 18, 'overflow_capacity': False},
            'Lite sted': {'bikes': 3, 'locks': 21, 'overflow_capacity': False},
            'Sted': {'bikes': 0, 'locks': 20, 'overflow_capacity': False},
            'Stort sted': {'bikes': 2, 'locks': 12, 'overflow_capacity': True},
        })
