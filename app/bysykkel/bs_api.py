import json
import urllib.request

API_URL = 'https://oslobysykkel.no/api/v1'
API_KEY = '' # Legg inn din Oslo bysykkel API nøkkel her

def _api_request(url):
    '''
        Returnerer en enkel GET urllib.request.Request med ferdig 
        utfylt API identifisering
    '''
    return urllib.request.Request(API_URL + url, headers={
        'Client_Identifier': API_KEY,
    }, method='GET')

def _getStationNames():
    '''
        Returnerer en dict med stasjonsnavn hvor man slår opp med 
        stasjonens ID

        {1: 'Sted', 2: 'Annet sted', ...}
    '''
    request = _api_request('/stations')

    result = dict()

    with urllib.request.urlopen(request) as response:
        data = response.read()

        stations = json.loads(data)['stations']

        for station in stations:
            result[station['id']] = station['title']

        response.close()

    return result

def _getAvailability():
    '''
        Returnerer en dict med stasjonenes sykkel- og låsinformasjon
        hvor man slår opp med stasjonens ID

        {1: {'bikes': 0, 'locks': 20, 'overflow_capacity': False}}
    '''
    request = _api_request('/stations/availability')

    result = dict()

    with urllib.request.urlopen(request) as response:
        data = response.read()

        stations = json.loads(data)['stations']

        for station in stations:
            result[station['id']] = station['availability']

        response.close()

    return result

def getStations():
    '''
        Returnerer en dict med sykkel- og låsinformasjon hvor man
        slår opp med stasjonens navn
    '''
    result = dict()

    names = _getStationNames()

    for _id, availability in _getAvailability().items():
        if _id in names:
            result[names[_id]] = availability

    return result

if __name__ == '__main__':
    '''
        Skriver ut en enkel liste med stasjonsnavn og tilgjengelighets-
        informasjon om hver stasjon
    '''
    template = '{:30}{bikes:5}\t{locks:5}'
    stations = getStations()

    print(template.format('Navn', bikes='Sykler', locks='Låser')) 
    for name, availability in stations.items():
        print(template.format(name, **availability))
