from django.http import JsonResponse

from lib.restapi.core import Entrypoint

from app.bysykkel import bs_api

class StationEP(Entrypoint):
    '''
        Eksponerer endepunktet /stations/:ID/
    '''
    stem = r'(?P<station_id>\d+)'

    def get(self, request, station_id):
        response = {'data': dict()}

        if station_id:
            stations = bs_api._getStationNames()

            _id = int(station_id)

            if _id in stations:
                title = stations[_id]
        
                response['data'][title] = bs_api.getStations()[title]
            else:
                response['msg'] = 'ID {} not found'.format(station_id)
        else:
            response['msg'] = 'Please supply a valid ID'

        return JsonResponse(response)

class Stations(Entrypoint):
    '''
        Eksponerer endepunktet /stations/
    '''
    includes = [StationEP,]

    def get(self, request):
        data = dict()
        for key, value in bs_api.getStations().items():
            data[key] = {
                'bikes': value['bikes'],
                'locks': value['locks'],
            }

        return JsonResponse({'data': data})
