from django.urls import re_path, include
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View

class Entrypoint(View):
    '''
        Defines a django develish rest entrypoint. 
        Usage:
            urlpatterns = [
                EntrypointSubclass.url(),
            ]
        
        Goal is to create a better structure for urls -> functionality and
        reduce repetition of code.

        In the Entrypoint subclass, define the rest verbs the entrypoint should
        expose. def get, post, patch, put, delete.
    '''
    @classmethod
    def __root_url(cls):
        return re_path(r'', cls.as_view())

    @classmethod
    def url(cls):
        '''
            Returns an url representing the entrypoint where suburls are 
            included
        '''
        decorators = getattr(cls, 'decorators', list())
        decorate = method_decorator(decorators, name='dispatch')

        decorate(cls)
        entrypoints = [cls.__root_url(),]

        for e in getattr(cls, 'includes', list()):
            decorate(e)
            entrypoints.insert(0, e.url())

        stem = getattr(cls, 'stem', cls.__name__.lower())

        return re_path(r'{}/'.format(stem), include(entrypoints))

    def http_method_not_allowed(self, request, *args, **kwargs):
        '''
            Default response for all methods that are not defined by
            the Entrypoint subclass
        '''
        return JsonResponse({
            'message': 'Method {m} not allowed for entrypoint {e}'.format(**{
                'm': request.method,
                'e': request.path
            })}, status=405)
